/* eslint-disable @typescript-eslint/dot-notation */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Movie } from 'src/app/api/models/movie.interface';
import { MoviesPageModule } from '../../movies.module';

import { EditMovieFormComponent } from './edit-movie-form.component';

describe('EditMovieFormComponent', () => {
  let component: EditMovieFormComponent;
  let fixture: ComponentFixture<EditMovieFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditMovieFormComponent],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MoviesPageModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EditMovieFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('setNewForm - form invalid when empty', () => {
    component.setNewForm();
    expect(component.formGroup.valid).toBeFalsy();
  });

  const mockDetailData = {
    id: 1,
    title: 'Dancing Lady',
    poster: 'http://dummyimage.com/400x600.png/cc0000/ffffff',
    genre: ['Comedy', 'Musical', 'Romance'],
    year: 2006,
    duration: 161,
    imdbRating: 8.27,
    actors: [4, 5, 6]
  };

  it('setEditForm - form invalid when empty', () => {
    component.setEditForm(mockDetailData);
    expect(component.formGroup.valid).toBeTruthy();
  });

  it('setNewForm - title field validity', () => {
    let errors = {};
    component.setNewForm();
    const title = component.formGroup.controls.title;
    errors = title.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it('setNewForm - check validators', () => {

    component.setNewForm();
    expect(component.formGroup.valid).toBeFalsy();


    component.formGroup.controls['title'].setValue('test');
    expect(component.formGroup.valid).toBeTruthy();

  });

  it('submitForm()', () => {
    const setDataSpy = spyOn(component, 'sendNewForm');

    component.setNewForm();
    expect(component.formGroup.valid).toBeFalsy();

    component.formGroup.controls['title'].setValue('test');
    expect(component.formGroup.valid).toBeTruthy();

    component.submitForm();
    expect(setDataSpy).toHaveBeenCalled();

  });


});
