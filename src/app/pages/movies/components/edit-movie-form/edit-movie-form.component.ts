import { Component, OnInit } from '@angular/core';
import { MoviesIntService } from 'src/app/services/movies-int.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Movie } from 'src/app/api/models/movie.interface';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-edit-movie-form',
  templateUrl: './edit-movie-form.component.html',
  styleUrls: ['./edit-movie-form.component.scss'],
})
export class EditMovieFormComponent implements OnInit {
  movie: number;
  formGroup: FormGroup;
  isSubmitted = false;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private moviesIntService: MoviesIntService,
    private loaderService: LoaderService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.movie = +this.activatedRoute.snapshot.paramMap.get('id');

    if (this.movie) {
      this.getEditForm();
    } else { this.setNewForm(); }
  }

  getEditForm() {
    this.getMovieData(this.movie);
  }

  setNewForm() {
    // const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    // Validators.pattern(urlRegex)
    this.formGroup = this.formBuilder.group({
      title: [null, Validators.required],
      poster: [null, []],
      genre: [[], []],
      actors: [[], []],
      company: [null, []],
      year: [null, []],
      duration: [null, []],
      imdbRating: [null, [Validators.min(0), Validators.max(10)]],
    });
  }

  setEditForm(movie: Movie) {
    // const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    // Validators.pattern(urlRegex)
    this.formGroup = this.formBuilder.group({
      title: [movie.title, Validators.required],
      poster: [movie.poster, []],
      genre: [movie.genre, []],
      actors: [movie.actors, []],
      company: [movie.company, []],
      year: [movie.year, []],
      duration: [movie.duration, []],
      imdbRating: [movie.imdbRating, [Validators.min(0), Validators.max(10)]],
    });
  }

  setCompany(event) {
    this.formGroup.controls.company.setValue(event);
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.formGroup.valid) {
      console.error('Form Invalid');
      return false;
    } else {
      if (this.movie) {
        this.sendEditForm(this.formGroup);
      } else { this.sendNewForm(this.formGroup); }
    }
  }

  sendEditForm(formGroup: FormGroup) {
    this.loaderService.simpleLoader();
    this.moviesIntService.updateItem(this.movie, formGroup.value).pipe(
      first()
    ).subscribe(
      (res: Movie) => {
        this.loaderService.dismissLoader();
        this.successToast();
        this.moviesIntService.reloadList();
        this.router.navigate(['movies', 'view', this.movie]);
      },
      error => {
        this.warningDialog();
        console.error('Error', error);
      }
    );
  }

  sendNewForm(formGroup: FormGroup) {
    this.loaderService.simpleLoader();
    this.moviesIntService.createItem(formGroup.value).pipe(
      first()
    ).subscribe(
      (res: Movie) => {
        this.loaderService.dismissLoader();
        this.successToast();
        this.moviesIntService.reloadList();
        this.router.navigate(['movies', 'view', res.id]);
      },
      error => {
        this.warningDialog();
        console.error('Error', error);
      }
    );
  }

  get errorControl() {
    return this.formGroup.controls;
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { this.navigateToMovies(); }
        }
      ]
    });
    await alert.present();
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: this.translate.instant('movies.toast.success.message'),
      duration: 2000
    });
    toast.present();
  }

  private getMovieData(movie): void {
    if (movie) {
      this.loaderService.simpleLoader();
      this.moviesIntService.getItem(movie).pipe(
        first()
      ).subscribe(
        (res: Movie) => {
          this.loaderService.dismissLoader();
          this.setEditForm(res);
        },
        error => {
          this.warningDialog();
          console.error('Error', error);
        }
      );
    }
  }

  private navigateToMovies(): void {
    this.router.navigate(['movies']);
  }

}
