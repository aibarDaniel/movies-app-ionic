import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';
import { Movie } from 'src/app/api/models/movie.interface';
import { MoviesIntService } from 'src/app/services/movies-int.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

@Component({
  selector: 'app-view-movie-buttons',
  templateUrl: './view-movie-buttons.component.html',
  styleUrls: ['./view-movie-buttons.component.scss'],
})
export class ViewMovieButtonsComponent implements OnInit {
  @Input() movieData: Movie;
  constructor(
    public alertController: AlertController,
    private router: Router,
    private translate: TranslateService,
    private moviesIntService: MoviesIntService,
    private loaderService: LoaderService,
    public toastController: ToastController) { }

  ngOnInit(): void { }

  editMovie(): void {
    // TODO - Edit Form Component
  }

  async deleteMovieDialog() {
    const alert = await this.alertController.create({
      cssClass: 'delete-dialog',
      header: this.translate.instant('movies.dialogs.delete.header'),
      message: this.translate.instant('movies.dialogs.delete.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.delete.cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => { }
        }, {
          text: this.translate.instant('movies.dialogs.delete.okay'),
          handler: () => { this.deleteMovie(); }
        }
      ]
    });

    await alert.present();
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { this.navigateToMovies(); }
        }
      ]
    });
    await alert.present();
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: this.translate.instant('movies.toast.success.message'),
      duration: 2000
    });
    toast.present();
  }

  private deleteMovie(): void {
    this.loaderService.simpleLoader();
    this.moviesIntService.deleteItem(this.movieData.id).pipe(first(),).subscribe(
      (res: Movie) => {
        this.loaderService.dismissLoader();
        this.successToast();
        this.moviesIntService.reloadList();
        this.navigateToMovies();
      },
      err => {
        this.loaderService.dismissLoader();
        this.warningDialog();
        console.error('Error when delete obteject', err);
      }
    );
  }

  private navigateToMovies(): void {
    this.router.navigate(['movies']);
  }

}
