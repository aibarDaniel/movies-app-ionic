import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { ViewMovieButtonsComponent } from './view-movie-buttons.component';

describe('ViewMovieButtonsComponent', () => {
  let component: ViewMovieButtonsComponent;
  let fixture: ComponentFixture<ViewMovieButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ViewMovieButtonsComponent],
      imports: [
        IonicModule.forRoot(),
        RouterModule.forRoot([]),
        HttpClientTestingModule,
        TranslateModule.forRoot(),
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewMovieButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
