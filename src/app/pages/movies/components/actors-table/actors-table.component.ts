import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { Actor } from 'src/app/api/models/actor.interface';
import { ActorsIntService } from 'src/app/services/actors-int.service';

@Component({
  selector: 'app-actors-table',
  templateUrl: './actors-table.component.html',
  styleUrls: ['./actors-table.component.scss'],
})
export class ActorsTableComponent implements OnInit, OnDestroy {
  @Input() actorsId: number[];

  actorsListNames: Actor[] = [];
  emptyData: boolean;
  loading: boolean;
  notifier = new Subject();

  constructor(private actorsIntService: ActorsIntService) { }

  ngOnInit(): void {
    this.getActors();
  }

  getActors(): void {
    this.loading = true;
    this.actorsIntService.actorsData
      .pipe(takeUntil(this.notifier)).subscribe(
        (res: Actor[]) => {
          if (res) {
            this.processActorsList(res);
            this.loading = false;
          }
        }, err => {
          console.error('Error getting Actors data: ', err);
          this.loading = false;
          this.actorsListNames = null;
        }
      );
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  private processActorsList(actorsList): void {
    this.actorsListNames = [];
    this.emptyData = false;
    this.actorsId.forEach(actorId => {
      const actor = actorsList.find(i => i.id === actorId);
      if (actor) { this.actorsListNames.push(actor); }
    });
    this.emptyData = !(this.actorsListNames?.length > 0);
  }

}
