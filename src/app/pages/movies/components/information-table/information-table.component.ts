import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/api/models/movie.interface';

@Component({
  selector: 'app-information-table',
  templateUrl: './information-table.component.html',
  styleUrls: ['./information-table.component.scss'],
})
export class InformationTableComponent implements OnInit {
  @Input() movieData: Movie;
  constructor() { }

  ngOnInit() { }

}
