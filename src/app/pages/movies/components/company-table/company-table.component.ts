import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Company } from 'src/app/api/models/company.interface';
import { Movie } from 'src/app/api/models/movie.interface';
import { CompaniesIntService } from 'src/app/services/companies-int.service';

@Component({
  selector: 'app-company-table',
  templateUrl: './company-table.component.html',
  styleUrls: ['./company-table.component.scss'],
})
export class CompanyTableComponent implements OnInit {
  @Input() companyId: number;

  listNames: Company[] = [];
  emptyData: boolean;
  loading: boolean;
  notifier = new Subject();

  constructor(private companiesIntService: CompaniesIntService) { }


  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.loading = true;
    this.companiesIntService.companiesData
      .pipe(takeUntil(this.notifier)).subscribe(
        (res: Company[]) => {
          if (res) {
            this.processList(res);
            this.loading = false;
          }
        }, err => {
          console.error('Error getting Actors data: ', err);
          this.loading = false;
          this.listNames = null;
        }
      );
  }

  private processList(actorsList): void {
    this.listNames = [];
    this.emptyData = false;
    const actor = actorsList.find(i => i.id === this.companyId);
    if (actor) { this.listNames.push(actor); }

    this.emptyData = !(this.listNames?.length > 0);
  }

}
