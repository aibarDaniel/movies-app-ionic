import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/api/models/movie.interface';

@Component({
  selector: 'app-hashtag-table',
  templateUrl: './hashtag-table.component.html',
  styleUrls: ['./hashtag-table.component.scss'],
})
export class HashtagTableComponent implements OnInit {
  @Input() movieData: Movie;
  constructor() { }

  ngOnInit() { }

}
