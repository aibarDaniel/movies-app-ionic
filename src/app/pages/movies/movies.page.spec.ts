import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MoviesPage } from './movies.page';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ViewMoviePage } from './view-movie/view-movie.page';
import { EditMoviePage } from './edit-movie/edit-movie.page';
import { MoviesButtonComponent } from './components/movies-button/movies-button.component';
import { ActorsTableComponent } from './components/actors-table/actors-table.component';
import { HashtagTableComponent } from './components/hashtag-table/hashtag-table.component';
import { ViewMovieButtonsComponent } from './components/view-movie-buttons/view-movie-buttons.component';
import { InformationTableComponent } from './components/information-table/information-table.component';
import { CompanyTableComponent } from './components/company-table/company-table.component';
import { EditMovieFormComponent } from './components/edit-movie-form/edit-movie-form.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('MoviesPage', () => {
  let component: MoviesPage;
  let fixture: ComponentFixture<MoviesPage>;

  const COMPONENTS = [
    MoviesPage, ViewMoviePage, EditMoviePage,
    MoviesButtonComponent, ActorsTableComponent,
    InformationTableComponent, HashtagTableComponent,
    CompanyTableComponent, ViewMovieButtonsComponent,
    EditMovieFormComponent
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [...COMPONENTS],
      imports: [IonicModule.forRoot(),
      RouterModule.forRoot([]),
        HttpClientTestingModule,
      TranslateModule.forRoot(),
        SharedModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MoviesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
