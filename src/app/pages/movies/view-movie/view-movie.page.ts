import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, Config, IonBackButtonDelegate, ViewDidEnter, ViewWillEnter } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError, finalize, first } from 'rxjs/operators';
import { Movie } from 'src/app/api/models/movie.interface';
import { MoviesIntService } from 'src/app/services/movies-int.service';
import { LoaderService } from 'src/app/shared/services/loader.service';

@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.page.html',
  styleUrls: ['./view-movie.page.scss'],
})
export class ViewMoviePage implements OnInit, ViewDidEnter {
  @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;

  public movie: string;
  public movieData: Movie;
  public movieHeaderTitle = '';

  constructor(
    public config: Config,
    public alertController: AlertController,
    private activatedRoute: ActivatedRoute,
    private moviesIntService: MoviesIntService,
    private loaderService: LoaderService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  ngOnInit(): void { }


  ionViewDidEnter(): void {
    this.movie = this.activatedRoute.snapshot.paramMap.get('id');
    this.getMovieData(this.movie);
    this.setUIBackButtonAction();
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { this.navigateToMovies(); }
        }
      ]
    });
    await alert.present();
  }

  setUIBackButtonAction() {
    this.backButton.onClick = () => {
      this.navigateToMovies();
    };
  }

  get viewActorsTable() {
    return this.movieData.actors && this.movieData.actors.length > 0;
  }

  get viewCompanyTable() {
    return this.movieData.company;
  }

  private getMovieData(movie): void {
    if (movie) {
      this.loaderService.simpleLoader();
      this.moviesIntService.getItem(movie).pipe(
        first(),
        catchError(() => of(null))
      ).subscribe(
        (res: Movie) => {
          this.loaderService.dismissLoader();
          this.processData(res);
        },
        error => {
          this.warningDialog();
          console.error('Error', error);
        }
      );
    }
  }

  private processData(movieData: Movie): void {
    if (movieData?.id) {
      this.movieData = movieData;

      const movieDate = movieData?.year ? ' (' + movieData.year + ')' : '';
      this.movieHeaderTitle = movieData.title + movieDate;
    } else {
      this.warningDialog();
    }
  }

  private navigateToMovies(): void {
    this.router.navigate(['movies']);
  }
}
