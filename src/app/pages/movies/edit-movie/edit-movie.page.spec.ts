import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MoviesPageModule } from '../movies.module';

import { EditMoviePage } from './edit-movie.page';

describe('EditMoviePage', () => {
  let component: EditMoviePage;
  let fixture: ComponentFixture<EditMoviePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditMoviePage],
      imports: [
        IonicModule.forRoot(),
        RouterTestingModule,
        MoviesPageModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        SharedModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EditMoviePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
