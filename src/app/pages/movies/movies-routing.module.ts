import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMoviePage } from './edit-movie/edit-movie.page';

import { MoviesPage } from './movies.page';
import { ViewMoviePage } from './view-movie/view-movie.page';

const routes: Routes = [
  {
    path: '',
    component: MoviesPage,
    children: [],
  },
  {
    path: 'view/:id',
    component: ViewMoviePage,
  },
  {
    path: 'edit',
    component: EditMoviePage,
  },

  {
    path: 'edit/:id',
    component: EditMoviePage,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesPageRoutingModule { }
