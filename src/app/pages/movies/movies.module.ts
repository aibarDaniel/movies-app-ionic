import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoviesPageRoutingModule } from './movies-routing.module';

import { MoviesPage } from './movies.page';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from 'src/app/shared/shared.module';
import { MoviesButtonComponent } from './components/movies-button/movies-button.component';
import { ViewMoviePage } from './view-movie/view-movie.page';
import { ActorsTableComponent } from './components/actors-table/actors-table.component';
import { InformationTableComponent } from './components/information-table/information-table.component';
import { HashtagTableComponent } from './components/hashtag-table/hashtag-table.component';
import { CompanyTableComponent } from './components/company-table/company-table.component';
import { ViewMovieButtonsComponent } from './components/view-movie-buttons/view-movie-buttons.component';
import { EditMoviePage } from './edit-movie/edit-movie.page';
import { EditMovieFormComponent } from './components/edit-movie-form/edit-movie-form.component';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const COMPONENTS = [
  MoviesPage, ViewMoviePage, EditMoviePage,
  MoviesButtonComponent, ActorsTableComponent,
  InformationTableComponent, HashtagTableComponent,
  CompanyTableComponent, ViewMovieButtonsComponent,
  EditMovieFormComponent
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MoviesPageRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [...COMPONENTS]
})
export class MoviesPageModule { }
