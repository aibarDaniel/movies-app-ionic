import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ViewWillEnter } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { of, Subject } from 'rxjs';
import { catchError, finalize, first, takeUntil } from 'rxjs/operators';
import { Movie } from 'src/app/api/models/movie.interface';
import { MoviesIntService } from 'src/app/services/movies-int.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit, OnDestroy {
  loading: boolean;
  moviesData: Movie[];

  notifier = new Subject();

  constructor(
    private moviesIntService: MoviesIntService,
    private router: Router,
    private translate: TranslateService,
    private alertController: AlertController
  ) { }

  ngOnInit(): void {
    this.getMoviesData();
    this.getSubscriptions();
  }

  getSubscriptions(): void {
    this.moviesIntService.reloadObservable.pipe(takeUntil(this.notifier)).subscribe((data) => {
      this.getMoviesData();
    });
  }

  refreshMovies(event): void {
    this.getMoviesData(event);
  }

  viewMovie(movie: Movie): void {
    this.router.navigate(['movies', 'view', movie.id]);
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { }
        }
      ]
    });
    await alert.present();
  }

  private getMoviesData(event?): void {
    this.loading = true;
    this.moviesIntService.getList().pipe(
      first(),
      finalize(() => this.loading = false)
    ).subscribe(
      (res: Movie[]) => {
        this.moviesData = res;
        if (event) { event.target.complete(); }
      },
      error => {
        console.error('Error', error);
        this.warningDialog();
        if (event) { event.target.complete(); }
      }
    );
  }

}
