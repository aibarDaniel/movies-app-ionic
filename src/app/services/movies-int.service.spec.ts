import { TestBed } from '@angular/core/testing';

import { MoviesIntService } from './movies-int.service';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';

describe('MoviesService', () => {
  let service: MoviesIntService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MoviesIntService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
