import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Movie } from '../api/models/movie.interface';
import { MoviesService } from '../api/services/movies.service';
import { LoaderService } from '../shared/services/loader.service';

@Injectable({
  providedIn: 'root'
})
export class MoviesIntService {
  public reloadObservable = new Observable((observer) => {
    observer.next();
    observer.complete();
  });
  private reloadSubject: Subject<boolean>;

  /*
   * Creates an instance of movies int service.
   * @param moviesService -
   */
  constructor(private moviesService: MoviesService) {

    /** Reload */
    this.reloadSubject = new Subject();
    this.reloadObservable = this.reloadSubject.asObservable();
  }

  reloadList(): void {
    this.reloadSubject.next(true);
  }

  // Get Movies data
  getList(): Observable<Movie[]> {
    return this.moviesService.getList();
  }

  // Get single Movie data by ID
  getItem(id: number): Observable<Movie> {
    return this.moviesService.getItem(id);
  }

  deleteItem(id: number): Observable<Movie> {
    return this.moviesService.deleteItem(id);
  }

  createItem(item: any): Observable<Movie> {
    return this.moviesService.createItem(item);
  }

  updateItem(id: number, item: any): Observable<Movie> {
    return this.moviesService.updateItem(id, item);
  }

}
