/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Company } from '../api/models/company.interface';
import { CompanyService } from '../api/services/company.service';

@Injectable({
  providedIn: 'root'
})
export class CompaniesIntService {

  private companies = new BehaviorSubject<Company[]>(null);
  readonly companiesData = this.companies.asObservable();

  constructor(private companiesService: CompanyService) {
    this.loadAll();
  }

  // Get Companies data
  getList(): Observable<Company[]> {
    return this.companiesService.getList();
  }

  // Get Companies data
  getFilteredList(filter?): Observable<Company[]> {
    const filterString = `?${filter ? 'name_like=' + filter + '&' : ''}_limit=20`;
    return this.companiesService.getFilteredList(filterString);
  }

  loadAll() {
    this.companiesService.getList().subscribe(
      (res: Company[]) => { this.companies.next(res); },
      err => { }
    );
  }

  updatedDataSelection(data: Company[]) {
    this.companies.next(data);
  }

}
