/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Actor } from '../api/models/actor.interface';
import { ActorService } from '../api/services/actor.service';

@Injectable({
  providedIn: 'root'
})
export class ActorsIntService {

  private actors = new BehaviorSubject<Actor[]>(null);
  readonly actorsData = this.actors.asObservable();

  constructor(private actorsService: ActorService) {
    this.loadAll();
  }

  // Get actors data
  getList(): Observable<Actor[]> {
    return this.actorsService.getList();
  }

  // Get filtered actors data
  getFilteredList(filter?: string): Observable<Actor[]> {
    let spaceText = [];
    let filterText = '';
    if (filter) {
      spaceText = filter.split(' ');
      if (spaceText.length === 1) {
        filterText = 'first_name_like=' + spaceText[0] + '&';
      }
      if (spaceText.length === 2) {
        filterText = 'first_name_like=' + spaceText[0] + '&last_name_like=' + spaceText[1] + '&';
      }
    }

    const filterString = `?${filterText !== '' ? filterText : ''}_limit=20`;
    return this.actorsService.getFilteredList(filterString);
  }

  loadAll() {
    this.actorsService.getList().subscribe(
      (res: Actor[]) => { this.actors.next(res); },
      err => { }
    );
  }

  updatedDataSelection(data: Actor[]) {
    this.actors.next(data);
  }

}
