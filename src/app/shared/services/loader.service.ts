import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {

    constructor(
        public loadingController: LoadingController,
        private translate: TranslateService
    ) { }

    // Simple loader
    simpleLoader() {
        this.loadingController.create({
            message: this.translate.instant('global.loading')
        }).then((response) => {
            response.present();
        });
    }

    // Dismiss loader
    dismissLoader() {
        this.loadingController.dismiss().then((response) => { }).catch((err) => {
            console.log('Error occured : ', err);
        });
    }

}
