import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';
import { Company } from 'src/app/api/models/company.interface';
import { CompaniesIntService } from 'src/app/services/companies-int.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-companies-search-modal',
  templateUrl: './companies-search-modal.component.html',
  styleUrls: ['./companies-search-modal.component.scss'],
})
export class CompaniesSearchModalComponent implements OnInit {
  @Input() title: string;
  @Input() service: CompaniesIntService;

  loadedData: Company[];
  loadedDataFilter: Company[];
  searchNotFound: boolean;
  filterSearch: boolean;
  constructor(
    public alertController: AlertController,
    public loaderService: LoaderService,
    private modalCtr: ModalController,
    private translate: TranslateService,
    private companiesIntService: CompaniesIntService
  ) { }

  ngOnInit() {
    this.searchData();
  }

  searchData(param?): void {
    this.loaderService.simpleLoader();
    this.companiesIntService.getFilteredList(param).pipe(first()).subscribe(
      (res: Company[]) => {
        this.loaderService.dismissLoader();
        this.loadedData = res;
      },
      error => {
        this.loaderService.dismissLoader();
        console.error('Error', error);
        this.warningDialog();
      }
    );
  }

  selectElement(id: number): void {
    this.close(id);
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { this.close(); }
        }
      ]
    });
    await alert.present();
  }

  async close(data?: number) {
    await this.modalCtr.dismiss(data ? data : null);
  }

  filterList(evt): void {
    this.searchNotFound = false;
    this.filterSearch = true;

    const searchTerm = evt.srcElement.value.toLocaleLowerCase();
    const filteredString = searchTerm.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

    this.companiesIntService.getFilteredList(filteredString).pipe(first()).subscribe(
      (res: Company[]) => {
        this.filterSearch = false;
        this.loadedDataFilter = res;
        this.searchNotFound = this.loadedDataFilter.length < 1;
      },
      error => {
        this.filterSearch = false;
        console.error('Error', error);
        this.warningDialog();
      }
    );
  }

}
