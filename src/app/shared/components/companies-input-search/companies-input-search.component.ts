import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Company } from 'src/app/api/models/company.interface';
import { CompaniesIntService } from 'src/app/services/companies-int.service';
import { CompaniesSearchModalComponent } from '../companies-search-modal/companies-search-modal.component';

@Component({
  selector: 'app-companies-input-search',
  templateUrl: './companies-input-search.component.html',
  styleUrls: ['./companies-input-search.component.scss'],
})
export class CompaniesInputSearchComponent implements OnInit {

  @Input() data: number;

  @Output() companiesEvent = new EventEmitter<number>();

  emptyData: boolean;
  loading: boolean;
  notifier = new Subject();

  companiesNamesList: Company[];
  dataWithNames: { id: number; name: string };

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    private companiesIntService: CompaniesIntService
  ) { }

  ngOnInit() {
    this.getCompanies();
  }

  getCompanies(): void {
    this.loading = true;
    this.companiesIntService.companiesData
      .pipe(takeUntil(this.notifier)).subscribe(
        (res: Company[]) => {
          if (res) {
            this.companiesNamesList = res;
            this.processTags();
            this.loading = false;
          }
        }, err => {
          console.error('Error getting Companies data: ', err);
          this.loading = false;
        }
      );
  }

  processTags(): void {
    if (this.data) {
      this.dataWithNames = {
        id: this.data,
        name: this.getNameById(this.data),
      };
    } else {
      this.dataWithNames = null;
    }

  }

  removeItem(): void {
    this.data = null;
    this.processTags();
    this.companiesEvent.emit(this.data);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CompaniesSearchModalComponent,
      componentProps: {
        title: 'movies.form.searchCompany',
        service: this.companiesIntService
      }
    });

    modal.onDidDismiss().then((modalDataResponse) => {
      if (modalDataResponse?.data !== null && modalDataResponse?.data !== undefined) {
        this.data = modalDataResponse.data;
        this.companiesEvent.emit(this.data);
        this.processTags();
      }
    });

    return await modal.present();
  }

  get isVisibleSkeleton() {
    return this.loading && this.data;
  }

  private getNameById(id: number): string {
    const company = this.companiesNamesList.find(i => i.id === id);
    return company.name;
  }

}
