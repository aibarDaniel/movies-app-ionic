import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';
import { Actor } from 'src/app/api/models/actor.interface';
import { ActorsIntService } from 'src/app/services/actors-int.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'app-actors-search-modal',
  templateUrl: './actors-search-modal.component.html',
  styleUrls: ['./actors-search-modal.component.scss'],
})
export class ActorsSearchModalComponent implements OnInit {

  @Input() title: string;
  @Input() service: ActorsIntService;

  loadedData: Actor[];
  loadedDataFilter: Actor[];
  searchNotFound: boolean;
  filterSearch: boolean;
  constructor(
    public alertController: AlertController,
    public loaderService: LoaderService,
    private modalCtr: ModalController,
    private translate: TranslateService,
    private actorsIntService: ActorsIntService
  ) { }

  ngOnInit() {
    this.searchData();
  }

  searchData() {
    this.loaderService.simpleLoader();
    this.actorsIntService.getList().pipe(first()).subscribe(
      (res: Actor[]) => {
        this.loaderService.dismissLoader();
        this.loadedData = res;
      },
      error => {
        this.loaderService.dismissLoader();
        console.error('Error', error);
        this.warningDialog();
      }
    );
  }

  selectElement(id: number) {
    this.close(id);
  }

  async warningDialog() {
    const alert = await this.alertController.create({
      cssClass: 'warning-dialog',
      header: this.translate.instant('movies.dialogs.error.header'),
      message: this.translate.instant('movies.dialogs.error.message'),
      buttons: [
        {
          text: this.translate.instant('movies.dialogs.error.accept'),
          handler: () => { this.close(); }
        }
      ]
    });
    await alert.present();
  }

  async close(data?: number) {
    await this.modalCtr.dismiss(data ? data : null);
  }

  filterList(evt) {
    this.searchNotFound = false;
    this.filterSearch = true;

    const searchTerm = evt.srcElement.value.toLocaleLowerCase();
    const filteredString = searchTerm.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

    this.actorsIntService.getFilteredList(filteredString).pipe(first()).subscribe(
      (res: Actor[]) => {
        this.filterSearch = false;
        this.loadedDataFilter = res;
        this.searchNotFound = this.loadedDataFilter.length < 1;
      },
      error => {
        this.filterSearch = false;
        console.error('Error', error);
        this.warningDialog();
      }
    );
  }

}
