import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Actor } from 'src/app/api/models/actor.interface';
import { ActorsIntService } from 'src/app/services/actors-int.service';
import { ActorsSearchModalComponent } from '../actors-search-modal/actors-search-modal.component';

@Component({
  selector: 'app-actors-input-search',
  templateUrl: './actors-input-search.component.html',
  styleUrls: ['./actors-input-search.component.scss'],
})
export class ActorsInputSearchComponent implements OnInit, OnDestroy {
  @Input() data: number[];

  @Output() actorsEvent = new EventEmitter<number[]>();

  emptyData: boolean;
  loading: boolean;
  notifier = new Subject();

  actorsNamesList: Actor[];
  dataWithNames: { id: number; name: string }[];

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    private actorsIntService: ActorsIntService
  ) { }

  ngOnInit() {
    this.getActors();
  }

  processTags(): void {
    this.dataWithNames = [];
    this.data.forEach(element => {
      this.dataWithNames.push({
        id: element,
        name: this.getNameById(element),
      });
    });
  }

  getActors(): void {
    this.loading = true;
    this.actorsIntService.actorsData
      .pipe(takeUntil(this.notifier)).subscribe(
        (res: Actor[]) => {
          if (res) {
            this.actorsNamesList = res;
            this.loading = false;
            this.processTags();

          }
        }, err => {
          console.error('Error getting Actors data: ', err);
          this.loading = false;
        }
      );
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ActorsSearchModalComponent,
      componentProps: {
        title: 'movies.form.searchActor',
        service: this.actorsIntService
      }
    });

    modal.onDidDismiss().then((modalDataResponse) => {
      if (modalDataResponse?.data !== null && modalDataResponse?.data !== undefined) {
        this.data.push(modalDataResponse.data);
        this.processTags();
      }
    });

    return await modal.present();
  }

  removeItem(i): void {
    this.data.splice(i, 1);
    this.processTags();
    this.actorsEvent.emit(this.data);
  }

  get isVisibleSkeleton() {
    return this.loading && this.data && this.data.length > 0;
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  private getNameById(id: number): string {
    const actor = this.actorsNamesList.find(i => i.id === id);
    return actor.first_name + ' ' + actor.last_name;
  }


}
