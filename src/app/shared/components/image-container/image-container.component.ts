import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/api/models/movie.interface';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss'],
})
export class ImageContainerComponent implements OnInit {
  @Input() movieData: Movie;
  constructor() { }

  ngOnInit() { }

}
