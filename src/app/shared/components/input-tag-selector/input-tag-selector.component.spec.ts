import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { InputTagSelectorComponent } from './input-tag-selector.component';

describe('InputTagSelectorComponent', () => {
  let component: InputTagSelectorComponent;
  let fixture: ComponentFixture<InputTagSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InputTagSelectorComponent],
      imports: [
        IonicModule.forRoot(),
        TranslateModule.forRoot(),]
    }).compileComponents();

    fixture = TestBed.createComponent(InputTagSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
