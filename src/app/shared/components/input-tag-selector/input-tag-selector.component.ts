import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-tag-selector',
  templateUrl: './input-tag-selector.component.html',
  styleUrls: ['./input-tag-selector.component.scss'],
})
export class InputTagSelectorComponent implements OnInit {
  @Input() title: string;
  @Input() placeholder: string;
  @Input() dataTags: string[];

  @Output() tagsEvent = new EventEmitter<string[]>();
  dataInput = '';

  constructor() { }

  ngOnInit() { }

  addTag(tag: string): void {
    this.dataTags.push(tag.replace(/\s/g, ''));
    this.dataInput = '';
    this.tagsEvent.emit(this.dataTags);
  }

  removeItem(i): void {
    this.dataTags.splice(i, 1);
    this.tagsEvent.emit(this.dataTags);
  }

}
