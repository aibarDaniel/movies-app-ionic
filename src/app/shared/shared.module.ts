import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CardComponent } from './components/card/card.component';
import { UiImageLoaderDirective } from './directives/not-image/image-loader.directive';
import { SkeletonComponent } from './components/skeleton/skeleton.component';
import { TimePipe } from './pipes/time.pipe';
import { InputTagSelectorComponent } from './components/input-tag-selector/input-tag-selector.component';
import { ActorsSearchModalComponent } from './components/actors-search-modal/actors-search-modal.component';
import { ActorsInputSearchComponent } from './components/actors-input-search/actors-input-search.component';
import { CompaniesInputSearchComponent } from './components/companies-input-search/companies-input-search.component';
import { CompaniesSearchModalComponent } from './components/companies-search-modal/companies-search-modal.component';
import { ImageContainerComponent } from './components/image-container/image-container.component';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const EXPORT_COMPONENTS = [
  CardComponent, SkeletonComponent,
  InputTagSelectorComponent, ActorsSearchModalComponent,
  ActorsInputSearchComponent, CompaniesInputSearchComponent,
  CompaniesSearchModalComponent, ImageContainerComponent];

const EXPORT_DIRECTIVES = [UiImageLoaderDirective];
const EXPORT_PIPES = [TimePipe];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [...EXPORT_COMPONENTS, ...EXPORT_DIRECTIVES, ...EXPORT_PIPES],
  exports: [...EXPORT_COMPONENTS, ...EXPORT_DIRECTIVES, ...EXPORT_PIPES]
})
export class SharedModule { }
