import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-panel-menu',
  templateUrl: './panel-menu.component.html',
  styleUrls: ['./panel-menu.component.scss'],
})
export class PanelMenuComponent implements OnInit {
  public appPages = [
    { title: 'movies.title', url: '/movies', icon: 'film' },
    { title: 'actors.title', url: '', icon: 'people' },
    { title: 'companies.title', url: '', icon: 'videocam' },
  ];
  constructor(
    public platform: Platform, private menu: MenuController) { }

  ngOnInit() { }

  openEnd() {
    this.menu.close();
  }

}
