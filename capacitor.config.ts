import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'movies-app-ionic',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
