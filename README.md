# IONIC MOVIE APP

This repository contains code to create a Movie Section in an IONIC Application

## Table of Contents

- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [Run locally](#run-locally)
- [Commits](#commits)
- [Deploy](#deploy)

## Getting Started

### Requirements

- Node.js [link](https://nodejs.org/en/)
- npm (With Node)
- git version control system [link](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Ionic CLI - [link](https://ionicframework.com/docs/intro/cli)

### Clone repository & install

Download the code:

```bash
# Get the latest snapshot
git clone git clone https://yourusername@bitbucket.org/aibarDaniel/movies-app-ionic.git

# Change directory
cd movies-app-ionic

# Install NPM dependencies
npm install
```

## Run locally

```bash
# If you are running the ionic app in a web browser
$ ionic serve

# OPTIONAL: This project contains JSON-SERVER library, if you want to run the 2 services directly you just need write:
$ npm run dev

```

## Commits

# Conventional Commits

https://www.conventionalcommits.org/

- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests

## Deploy

```bash
# Build the project
$ ionic build

# Create a Android Project
$ npx cap add android

# Create a IOS Project
$ npx cap add ios

# After making updates to the native portion of the code (such as adding a new plugin), use the sync command:
$ npx cap sync

# OPTIONAL: If the project exist and need add changes
$ npx cap copy android

# OPTIONAL: If the project exist and need add changes
$ npx cap copy ios

# Open in Android Studio
$ npx cap open android

# Open in XCode
$ npx cap open ios

```

## Coding recommendations

### Integrated development environment

App developed with Visual studio code [link](https://code.visualstudio.com/).
Plugins used for Visual Studio Code:

- Angular Language Service
- angular2-inline
- Bracket Pair Colorizer 2
- GitLens
- jshint
- TypeScript Hero
- TypeScript Importer
- vscode-icons

### Standarts

- Write comments and documentation
- Write readable yet efficient code
- Write tests
- SOLID principles [more info](https://en.wikipedia.org/wiki/SOLID)
- Follow lint recommendations
